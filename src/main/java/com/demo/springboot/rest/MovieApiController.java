package com.demo.springboot.rest;

import com.demo.springboot.MovieApi;
import com.demo.springboot.dto.CreateMovieDto;
import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieInfoDto;
import com.demo.springboot.dto.MovieListDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@RestController
public class MovieApiController {
    MovieApi movieApi = new MovieApi();

    public MovieApiController() {
    }

    @PostMapping("/api/users")
    public ResponseEntity<MovieInfoDto> addMovie(@RequestBody MovieInfoDto movieInfoDto) throws URISyntaxException {
        return movieApi.addMovie(movieInfoDto);
    }

    @PutMapping("/api/users/{id}")
    public ResponseEntity<Void> updateMovie(@PathVariable("id") Integer id, @RequestBody MovieInfoDto movieInfoDto) {
       return movieApi.updateMovie(id,movieInfoDto);
    }
    @DeleteMapping("/api/users/{id}")
    public ResponseEntity<Void> deleteMovie(@PathVariable("id") Integer id){
        return movieApi.deleteMovie(id);
    }

    @GetMapping("/api/users")
    public ResponseEntity<MovieListDto> getMovies() {
        return movieApi.getMovies();
    }


}
