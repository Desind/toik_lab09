package com.demo.springboot;

import com.demo.springboot.dto.MovieDto;
import com.demo.springboot.dto.MovieInfoDto;
import com.demo.springboot.dto.MovieListDto;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class MovieApi {
    private MovieListDto movies;
    private int currentId = 1;
    List<MovieDto> moviesList = new ArrayList<>();

    public MovieApi() {
        moviesList.add(new MovieDto(0,
                "Piraci z Krzemowej Doliny",
                1999,
                "https://fwcdn.pl/fpo/30/02/33002/6988507.6.jpg")
        );
        movies = new MovieListDto(moviesList);
    }
    public ResponseEntity<MovieInfoDto> addMovie(MovieInfoDto movieInfoDto){
        if (movieInfoDto.getImage() == null || movieInfoDto.getTitle() == null || movieInfoDto.getYear() == null) {
            return ResponseEntity.status(400).build();
        } else {
            moviesList.add(new MovieDto(currentId++,
                    movieInfoDto.getTitle(),
                    movieInfoDto.getYear(),
                    movieInfoDto.getImage())
            );
            movies = new MovieListDto(moviesList);
            return ResponseEntity.status(201).build();
        }
    }
    public ResponseEntity<Void> updateMovie(Integer id, MovieInfoDto movieInfoDto){
        MovieDto movieDto;
        try{
            movieDto = movies.getMovie(id);
            movieDto.setImage(movieInfoDto.getImage());
            movieDto.setTitle(movieInfoDto.getTitle());
            movieDto.setYear(movieInfoDto.getYear());
            return ResponseEntity.ok().build();
        }catch(IndexOutOfBoundsException e){
            return ResponseEntity.notFound().build();
        }
    }
    public ResponseEntity<Void> deleteMovie(Integer id){
        int indexToDelete = -1;
        for(int i=0; i<moviesList.size(); i++){
            MovieDto m = moviesList.get(i);
            if(m.getMovieId()==id){
                indexToDelete = i;
                break;
            }
        }
        if(indexToDelete == -1){
            return ResponseEntity.notFound().build();
        }else{
            moviesList.remove(indexToDelete);
            return ResponseEntity.status(200).build();
        }
    }
    public ResponseEntity<MovieListDto> getMovies() {
        List<MovieDto> mv = new ArrayList<>();
        mv.addAll(moviesList);
        Collections.reverse(mv);
        MovieListDto mldto = new MovieListDto(mv);
        return ResponseEntity.ok().body(mldto);
    }
}
